import React from 'react';
import styled from 'styled-components';
import { Row, Col } from 'antd';
import { NavLink } from 'react-router-dom';

const CrumbsWrapper = styled.div`
  h4 {
    color: #23dcc8;
  }
`;

const Crumbs = (props) => {
  const { currentPath, currentStaker } = props;
  let endpoint;
  console.log('---', props);
  if (currentPath && currentPath.includes(`/v1/stakers/${currentStaker}`)) {
    if (currentPath.includes('/pools')) {
      endpoint = `/v1/stakers/${currentStaker}`;
    } else {
      endpoint = '/v1/stakers';
    }
  } else {
    return null;
  }
  return (
    <CrumbsWrapper>
      <Row>
        <Col xs={24} sm={11} md={10} lg={9}>
          <NavLink to={`${endpoint}`}>
            <h4>{endpoint}</h4>
          </NavLink>
        </Col>
      </Row>
    </CrumbsWrapper>
  );
};

export default Crumbs;
