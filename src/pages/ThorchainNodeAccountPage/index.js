import React from 'react';
import ThorchainNodeAccountPageWrapper from './index.style';
import { NavLink } from 'react-router-dom';

const ThorchainNodeAccountPage = () => {
  return (
    <ThorchainNodeAccountPageWrapper>
      <NavLink className="link__button" to="/thorchain/nodeaccounts">
        &lt;&nbsp;Node Accounts
      </NavLink>
    </ThorchainNodeAccountPageWrapper>
  );
};

export default ThorchainNodeAccountPage;
