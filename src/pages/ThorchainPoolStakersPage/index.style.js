import styled from 'styled-components';

const ThorchainPoolStakersPageWrapper = styled.div`
  margin-top: 20px;
  .link__pool {
    font-family: Open Sans;
    font-size: 14px;
    color: rgb(255, 255, 255);
    display: flex;
    justify-content: center;
    align-items: center;
    height: 40px;
    width: 300px;
    border-radius: 9px;
    background-color: rgb(0, 0, 0);
    border: 1px solid rgb(79, 225, 196);
  }
`;

export default ThorchainPoolStakersPageWrapper;
