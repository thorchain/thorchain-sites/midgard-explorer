import React from 'react';
import ThorchainPoolStakersPageWrapper from './index.style';
import { NavLink } from 'react-router-dom';

const ThorchainPoolStakersPage = (props) => {
  const {
    match: {
      params: { id: assetId },
    },
  } = props;
  const backPath = `/thorchain/pool/${assetId}`;

  return (
    <ThorchainPoolStakersPageWrapper>
      <NavLink className="link__pool" to={backPath}>
        {backPath}
      </NavLink>
    </ThorchainPoolStakersPageWrapper>
  );
};

export default ThorchainPoolStakersPage;
