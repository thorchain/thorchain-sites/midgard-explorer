import styled from 'styled-components';

const ThorchainPoolsPageWrapper = styled.div`
  & > div {
    margin-top: 5px;
  }
  .link__pool {
    font-family: Open Sans;
    font-size: 14px;
    color: rgb(255, 255, 255);
    display: flex;
    justify-content: center;
    align-items: center;
    height: 40px;
    width: 250px;
    border-radius: 9px;
    background-color: rgb(0, 0, 0);
    border: 1px solid rgb(79, 225, 196);
  }
`;

export default ThorchainPoolsPageWrapper;
