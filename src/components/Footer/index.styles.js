import styled from 'styled-components';

const FooterWrapper = styled.div`
  .footer-container {
    background-color: #2b3947;
    text-transform: uppercase;
    z-index: 1;
    left: 0;
    bottom: 0;
    right: 0;
    padding-top: 10px;
    padding-bottom: 10px;
    padding-left: 10px;
    padding-right: 10px;
    text-align: left;
    font-size: 18px;
  }
`;

export default FooterWrapper;
