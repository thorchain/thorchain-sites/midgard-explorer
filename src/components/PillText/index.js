import React from 'react';

const defaultStyle = {
  fontFamily: 'Open Sans',
  fontSize: '14px',
  color: '#FFFFFF',
  letterSpacing: 0,
};

const PillText = (props) => {
  let style = { ...defaultStyle, ...(props.style || {}) };
  style.backgroundColor = '#4FE1C4';
  style.borderRadius = 28;
  style.padding = '8px 20px';
  style.fontSize = '14px';
  return <span style={style}>{props.children}</span>;
};

export default PillText;
