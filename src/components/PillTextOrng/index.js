import React from 'react';

const defaultStyle = {
  fontFamily: 'Open Sans',
  fontSize: '14px',
  color: '#FFFFFF',
  letterSpacing: 0,
};

const PillTextOrng = (props) => {
  let style = { ...defaultStyle, ...(props.style || {}) };
  style.backgroundColor = '#4FE1C4';
  style.borderColor = '#33CCFF';
  style.borderRadius = 28;
  style.padding = '8px 20px';
  style.fontSize = '14px';
  style.color = '#FFF';
  return <span style={style}>{props.children}</span>;
};

export default PillTextOrng;
