import React from 'react';
import PropTypes from 'prop-types';

const defaultStyle = {
  fontFamily: 'Open Sans',
  fontSize: '14px',
  color: '#FFFFFF',
  letterSpacing: 0,
  cursor: 'pointer',
};

const Text = (props) => {
  let style = { ...defaultStyle, ...(props.style || {}) };
  if (props.bold) {
    style.fontFamily = 'Exo 2';
  }
  if (props.color) {
    style.color = props.color;
  }
  if (props.size) {
    style.fontSize = props.size;
  }
  return <span style={style}>{props.children}</span>;
};

Text.propTypes = {
  fontSize: PropTypes.string,
  bold: PropTypes.bool,
  color: PropTypes.string,
};

export default Text;
