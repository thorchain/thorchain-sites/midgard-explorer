import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const CenterWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`;

const Center = (props) => <CenterWrapper>{props.children}</CenterWrapper>;

Center.propTypes = {
  children: PropTypes.node,
};

Center.defaultProps = {
  children: null,
};

export default Center;
