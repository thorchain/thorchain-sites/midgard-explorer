import styled from 'styled-components';

const HeaderWrapper = styled.div`
  .header-container {
    background: #2b3947;
    box-shadow: 0 2px 16px 0 rgba(0, 0, 0, 0.09);

    z-index: 1;
    width: 100%;
    background-color: #2b3947;

    display: flex;
    align-items: center;

    & > div {
      flex: 1;
    }

    .section__base_url {
      text-align: right;
    }
  }
`;

export default HeaderWrapper;
