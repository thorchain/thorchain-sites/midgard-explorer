import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
    body {
        font-family: 'Open Sans', sans-serif;
        font-size: 18px;
        color: #ffffff;
        background-color: '#101921';
        letter-spacing: 0;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
    }

    code {
    font-family: source-code-pro, Menlo, Monaco, Consolas, 'Courier New',
        monospace;
    }
    .ant-menu-item-selected > a,
    .ant-menu-item-selected > a:hover {
    color: inherit;
    }

    .ant-menu-item > a:hover {
    color: rgba(0, 0, 0, 0.65);
    }

    div.ant-row.ant-form-item.form-100 > div {
    width: 98%;
    }

    .ant-menu-inline,
    .ant-menu-vertical,
    .ant-menu-vertical-left {
    border: none;
    }

    .button {
    background: #f0b90b;
    border-radius: 9px;
    font-family: 'Exo 2', sans-serif;
    letter-spacing: 2.75px;
    font-size: 18px;
    color: #ffffff;
    letter-spacing: 0;
    text-align: center;
    }

    .pill {
    background: #ededed;
    border-radius: 28px;
    font-family: 'Open Sans', sans-serif;
    font-size: 18px;
    color: #848e9c;
    letter-spacing: 0;
    }

    .text-box-area {
    border: 1px solid #f0b90b;
    border-radius: 6px;
    }

    .text-box-input {
    background: #f8f8f8;
    border: 1px solid #848e9c;
    border-radius: 8px;
    font-family: 'Open Sans', sans-serif;
    font-size: 18px;
    color: #848e9c;
    letter-spacing: 0;
    line-height: 23px;
    }

    .h1 {
    font-family: 'Exo 2', sans-serif;
    letter-spacing: 2.75px;
    font-size: 42px;
    color: #ffffff;
    }

    .text {
    font-family: 'Exo 2', sans-serif;
    letter-spacing: 2.75px;
    font-size: 18px;
    color: #ffffff;
    }

    .text-bold {
    font-family: 'Exo 2', sans-serif;
    letter-spacing: 2.75px;
    font-size: 18px;
    color: #ffffff;
    letter-spacing: 0;
    }

    .text-bg {
        background: #ffffff;
    }

    .card-container > .ant-tabs-card > .ant-tabs-bar {
        border-color: #50e3c2;
    }
    /* scrollbar */
    ::-webkit-scrollbar {
    }
    
    ::-webkit-scrollbar-track {
        box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
    }
    
    ::-webkit-scrollbar-thumb {
        background-color: #fff8;
        outline: 1px solid #fff8;
        border-radius: 3px;
    }
`;

export default GlobalStyle;
