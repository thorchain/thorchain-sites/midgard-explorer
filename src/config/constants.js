export default {
  API_MIDGARD: 'midgard',
  API_THORCHAIN: 'thorchain',
  KEY_API: 'api',
  KEY_ENV: 'env',
  API_ENV_TEST: 'testnet',
  API_ENV_MAIN: 'mainnet',
  THORCHAIN_ENDPOINTS: [
    { id: 'constants', label: 'Constants', endPoint: '/thorchain/constants' },
    {
      id: 'pool_addresses',
      label: 'Pool Addresses',
      endPoint: '/thorchain/pool_addresses',
    },
    {
      id: 'outqueue',
      label: 'OutQueue',
      endPoint: '/thorchain/out/queue',
    },
    {
      id: 'mimirs',
      label: 'Mimirs',
      endPoint: '/thorchain/mimir',
    },
    {
      id: 'ragnarok',
      label: 'Ragnarok',
      endPoint: '/thorchain/ragnarok',
    },
    {
      id: 'transaction',
      label: 'Transaction',
      endPoint: '/thorchain/tx',
    },
    {
      id: 'pools',
      label: 'Pools',
      endPoint: '/thorchain/pools',
      secondaryEndPoint: '/thorchain/pool',
    },
    {
      id: 'lastblock',
      label: 'Last Block',
      endPoint: '/thorchain/lastblock',
    },
    {
      id: 'observers',
      label: 'Observers',
      endPoint: '/thorchain/observers',
    },
    {
      id: 'nodeaccounts',
      label: 'Node Accounts',
      endPoint: '/thorchain/nodeaccounts',
      secondaryEndPoint: '/thorchain/nodeaccount',
    },
  ],
  THORCHAIN_DEFAULT_ENDPOINT: '/thorchain/constants',
  THORCHAIN_SHOW_PATHS: ['/thorchain/tx'],
  MIDGARD_DEFAULT_ENDPOINT: '/v1/health',
};
