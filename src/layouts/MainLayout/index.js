import React, { useContext } from 'react';
import { Layout } from 'antd';
import MainLayoutWrapper from './index.styles';
import { Header, Footer, Loader } from 'components';
import { useApiData } from 'utils/helpers';
import ResponseLayout from 'layouts/ResponseLayout';
import { NavBar } from './components';
import ApiContext from 'context/ApiContext';
import constants from 'config/constants';

const { Content } = Layout;

const MainLayout = () => {
  const { apiType } = useContext(ApiContext);
  const isMidgard = apiType === constants.API_MIDGARD;
  const endPoint = isMidgard
    ? constants.MIDGARD_DEFAULT_ENDPOINT
    : constants.THORCHAIN_DEFAULT_ENDPOINT;
  const { midgardPaths, node } = useApiData(endPoint);

  const renderRoutes = () => {
    if (!node || (isMidgard && !midgardPaths)) {
      return <Loader />;
    }
    return <ResponseLayout />;
  };

  return (
    <MainLayoutWrapper>
      <Layout className="layout__top">
        <Header />
        <Layout className="main_content">
          <NavBar midgardPaths={midgardPaths} node={node} />
          <Layout className="page_layout">
            <Content className="page_content">{renderRoutes()}</Content>
          </Layout>
        </Layout>
        <Footer />
      </Layout>
    </MainLayoutWrapper>
  );
};

export default MainLayout;
