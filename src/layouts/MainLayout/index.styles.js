import styled from 'styled-components';

const MainLayoutWrapper = styled.div`
  .layout__top {
    background-color: #101921;
    min-height: 100vh;
    max-height: 100vh;
    .main_content {
      .page_layout {
        background-color: #101921;
        .page_content {
          overflow: auto;
          margin: 10px;
          margin-left: 0;
          border-radius: 5px;
          background-color: #2b3947;
          padding: 5px;
          position: relative;
        }
      }
    }
  }
`;

export default MainLayoutWrapper;
