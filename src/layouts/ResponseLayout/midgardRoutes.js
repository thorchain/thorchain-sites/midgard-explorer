import React from 'react';
import PoolListPage from 'pages/PoolListPage';
import StakerListPage from 'pages/StakerListPage';
import StakerPoolPage from 'pages/StakerPoolPage';

const midgardRoutes = [
  {
    path: '/v1/pools',
    exact: true,
    component: PoolListPage,
  },
  {
    path: '/v1/stakers',
    exact: true,
    component: StakerListPage,
  },
  {
    path: '/v1/stakers/:stakerId',
    exact: false,
    component: StakerPoolPage,
  },
];

export default midgardRoutes;
