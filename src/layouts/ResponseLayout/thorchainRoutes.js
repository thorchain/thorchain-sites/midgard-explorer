import React from 'react';
import ThorchainTransactionPage from 'pages/ThorchainTransactionPage';
import ThorchainPoolsPage from 'pages/ThorchainPoolsPage';
import ThorchainPoolPage from 'pages/ThorchainPoolPage';
import ThorchainPoolStakersPage from 'pages/ThorchainPoolStakersPage';
import ThorchainLastBlockPage from 'pages/ThorchainLastBlockPage';
import ThorchainNodeAccountsPage from 'pages/ThorchainNodeAccountsPage';
import ThorchainNodeAccountPage from 'pages/ThorchainNodeAccountPage';

const thorchainRoutes = [
  {
    path: '/thorchain/tx',
    exact: true,
    component: ThorchainTransactionPage,
  },
  {
    path: '/thorchain/tx/:txId',
    exact: true,
    component: ThorchainTransactionPage,
  },
  {
    path: '/thorchain/pools',
    exact: true,
    component: ThorchainPoolsPage,
  },
  {
    path: '/thorchain/pool/:id',
    exact: true,
    component: ThorchainPoolPage,
  },
  {
    path: '/thorchain/pool/:id/stakers',
    exact: true,
    component: ThorchainPoolStakersPage,
  },
  {
    path: '/thorchain/lastblock',
    exact: false,
    component: ThorchainLastBlockPage,
  },
  {
    path: '/thorchain/nodeaccounts',
    exact: true,
    component: ThorchainNodeAccountsPage,
  },
  {
    path: '/thorchain/nodeaccount/:id',
    exact: true,
    component: ThorchainNodeAccountPage,
  },
];

export default thorchainRoutes;
