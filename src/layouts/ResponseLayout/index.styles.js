import styled from 'styled-components';

const ResponseLayoutWrapper = styled.div`
  height: 100%;
  overflow-y: auto;
  .response__content {
    font-family: Courier New;
  }
`;

export default ResponseLayoutWrapper;
