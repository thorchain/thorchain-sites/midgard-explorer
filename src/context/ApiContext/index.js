import { createContext } from 'react';

const defaultValue = {
  baseUrl: '',
  apiType: '',
  env: '',
  setBaseUrl: () => {},
  setApiType: () => {},
  setEnv: () => {},
};

const ApiContext = createContext(defaultValue);

export default ApiContext;
