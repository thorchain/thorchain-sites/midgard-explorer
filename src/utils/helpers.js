import { useState, useEffect, useContext } from 'react';
import { __RouterContext, matchPath } from 'react-router';
import axios from 'axios';
import ApiContext from 'context/ApiContext';
import constants from 'config/constants';

var midgardPaths;

export const useApiData = (endpoint) => {
  const { baseUrl, apiType } = useContext(ApiContext);
  const router = useRouter();
  const blackList = ['/v1/doc', '/v1/swagger.json'];

  const [state, setState] = useState({
    midgardPaths: midgardPaths,
    data: null,
    node: null,
    loading: true,
  });

  useEffect(() => {
    setState({
      midgardPaths: midgardPaths,
      data: null,
      node: state.node ? state.node : null,
      loading: true,
    });

    const fetchThorchain = async () => {
      if (!baseUrl) return;
      try {
        const isShowOnlyPath = constants.THORCHAIN_SHOW_PATHS.map((path) =>
          matchPath(endpoint, { path, exact: true })
        ).reduce((e1, e2) => e1 && e2);
        if (isShowOnlyPath) {
          setState({
            data: '',
            node: baseUrl,
            prevPath: state.currentPath,
            currentPath: endpoint,
            loading: false,
          });
          return;
        }
        const res = await axios.get(endpoint, { baseURL: baseUrl });
        setState({
          data: res.data,
          node: baseUrl,
          prevPath: state.currentPath,
          currentPath: endpoint,
          loading: false,
        });
      } catch (error) {
        setState({
          data: 'Error fetching data...',
          node: baseUrl,
          loading: false,
        });
        router.history.push(constants.THORCHAIN_DEFAULT_ENDPOINT);
      }
    };

    const isDirty = (object, path) => {
      return !blackList.find((e) => e === path) && !object[path].get.parameters
        ? false
        : true;
    };

    const fetchMidgardPaths = async () => {
      setState({ node: baseUrl });
      try {
        let res = await axios.get('/v1/swagger.json', { baseURL: baseUrl });
        let data = res.data.paths;
        let allPaths = Object.keys(data).map((key) => key);
        midgardPaths = allPaths.filter((path) => !isDirty(data, path));
      } catch (error) {}
    };

    const fetchMidgard = async () => {
      if (!baseUrl) return;
      if (!midgardPaths) {
        await fetchMidgardPaths();
      }

      try {
        const res = await axios.get(endpoint, { baseURL: baseUrl });

        let staker =
          endpoint && endpoint.includes('/v1/stakers/')
            ? endpoint.split('/')[3]
            : null;
        setState({
          midgardPaths: midgardPaths,
          data: res.data,
          currentStaker: staker,
          node: baseUrl,
          prevPath: state.currentPath,
          currentPath: endpoint,
          loading: false,
        });
      } catch (e) {
        setState({
          midgardPaths: midgardPaths,
          data: 'Error fetching data...',
          node: baseUrl,
          loading: false,
        });
        router.history.push(constants.MIDGARD_DEFAULT_ENDPOINT);
      }
    };

    if (apiType === constants.API_MIDGARD) fetchMidgard();
    else fetchThorchain();
  }, [endpoint, baseUrl]);

  return state;
};

export const useRouter = () => useContext(__RouterContext);
